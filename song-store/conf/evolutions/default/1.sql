# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table artista (
  id                        bigint auto_increment not null,
  name                      varchar(255),
  
  constraint pk_artista primary key (id))
;




# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table artista;

SET FOREIGN_KEY_CHECKS=1;

