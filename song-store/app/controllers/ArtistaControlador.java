package controllers;


import java.awt.print.Book;
import java.util.Set;

import javax.inject.Inject;

import models.Artista;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.artista.*;

public class ArtistaControlador extends Controller {

	
	@Inject
	FormFactory formFactory; 
	
	// Listar canciones
	
	public Result index(){
		Set<Artista> artistas = Artista.todosArtistas(); 
		return ok(index.render(artistas));
		
		
	}
	
	//
	public Result crear(){
		Form<Artista> artistaForm = formFactory.form(Artista.class);  
		return ok(crear.render(artistaForm));
		
	}
	
	// Guardar artista
	public Result guardar(){
		Form<Artista> artistaForm = formFactory.form(Artista.class).bindFromRequest();
		Artista artista = artistaForm.get();
		Artista.add(artista);
		return redirect(routes.ArtistaControlador.index());
		
	}
	
}	
	
	
	

